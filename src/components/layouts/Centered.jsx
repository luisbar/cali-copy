import React from 'react'
import propTypes from 'prop-types'
import Image from '../atoms/Image'

const Centered = (
  { image, body }
) => {

  return (
    <div
      className='centered'
    >
      <Image
        imageName={image}
      />
      {body}
    </div>
  )

}

Centered.propTypes = {
  image: propTypes.string,
  body: propTypes.node.isRequired,
}

Centered.defaultProps = {
  image: '',
}

export default Centered
