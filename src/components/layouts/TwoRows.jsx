import React from 'react'
import propTypes from 'prop-types'
import Image from '../atoms/Image'

const TwoRows = (
  {
    firstRowLeftContent,
    firstRowRightContent,
    secondRowImage,
    secondRowContent,
  }
) => {

  return (
    <div
      className='twoRows'
    >
      <div
        className='first'
      >
        <div
          className='left'
        >
          {firstRowLeftContent}
        </div>
        <div
          className='right'
        >
          {firstRowRightContent}
        </div>
      </div>
      <div
        className='second'
      >
        <Image
          imageName={secondRowImage}
        />
        {secondRowContent}
      </div>
    </div>
  )

}

TwoRows.propTypes = {
  firstRowLeftContent: propTypes.node.isRequired,
  firstRowRightContent: propTypes.node.isRequired,
  secondRowImage: propTypes.string,
  secondRowContent: propTypes.node.isRequired,
}

TwoRows.defaultProps = {
  secondRowImage: '',
}

export default TwoRows
