import React from 'react'
import propTypes from 'prop-types'
import Image from '../atoms/Image'

const HeaderAndBody = (
  { header, body, bodyBackground }
) => {

  return (
    <div
      className='headerAndBody'
    >
      <div
        className='header'
      >
        {header}
      </div>
      <div
        className='body'
      >
        <Image
          imageName={bodyBackground}
        />
        {body}
      </div>
    </div>
  )

}

HeaderAndBody.propTypes = {
  header: propTypes.node.isRequired,
  body: propTypes.node.isRequired,
  bodyBackground: propTypes.node,
}

HeaderAndBody.defaultProps = {
  bodyBackground: '',
}

export default HeaderAndBody
