import React from 'react'
import propTypes from 'prop-types'

const UnderlinedTitle = (
  { label }
) => {

  return (
    <div
      className='underlinedTitle'
    >
      <h3>
        {label}
      </h3>
      <div />
    </div>
  )

}

UnderlinedTitle.propTypes = {
  label: propTypes.string.isRequired,
}

export default UnderlinedTitle
