import React from 'react'
import propTypes from 'prop-types'
import Menu from '../molecules/Menu'
import Image from '../atoms/Image'

const Toolbar = (
  {
    defaultLang,
    langKey,
    langTextMap,
    pageIds,
    multiplePages,
    elementInViewPort,
    desktopLogo,
  }
) => {

  return (
    <div
      className='toolbar'
    >
      <div
        className='left'
      >
        <Image
          imageName={desktopLogo}
        />
      </div>

      <div
        className='right'
      >
        <Menu
          items={pageIds}
          multiplePages={multiplePages}
          elementInViewPort={elementInViewPort}
          defaultLang={defaultLang}
          langKey={langKey}
          langTextMap={langTextMap}
        />
      </div>
    </div>
  )

}

Toolbar.propTypes = {
  defaultLang: propTypes.string.isRequired,
  langKey: propTypes.string.isRequired,
  langTextMap: propTypes.object.isRequired,
  pageIds: propTypes.object,
  multiplePages: propTypes.bool,
  elementInViewPort: propTypes.string.isRequired,
  desktopLogo: propTypes.string.isRequired,
}

Toolbar.defaultProps = {
  pageIds: {},
  multiplePages: false,
}

export default Toolbar
