import React from 'react'
import PropTypes from 'prop-types'
import CardMember from '../molecules/CardMember'

const CardMembers = (
  { items }
) => {

  return (
    <div
      className='cardMembers'
    >
      {items.map(
        (
          item
        ) => {

          return (
            <CardMember
              key={item.name}
              {...item}
            />
          )

        }
      )}
    </div>
  )

}

CardMembers.propTypes = {
  items: PropTypes.arrayOf(
    PropTypes.shape(
      CardMember.propTypes
    ).isRequired
  ).isRequired,
}

export default CardMembers
