import React, { useRef, useState } from 'react'
import propTypes from 'prop-types'
import Title from '../atoms/Title'
import Paragraph from '../atoms/Paragraph'
import NormalButton from '../atoms/NormalButton'
import Input from '../molecules/Input'

const ContactForm = (
  {
    formTitle,
    nameInputLabel,
    nameInputError,
    messageInputLabel,
    messageInputError,
    sendButtonLabel,
    email,
  }
) => {

  const nameInputRef = useRef()
  const messageInputRef = useRef()
  const [ showNameInputError, setShowNameInputError ] = useState(
    false
  )
  const [ showMessageInputError, setShowMessageInputError ] = useState(
    false
  )

  const onSend = () => {

    if (!nameInputRef.current.value) {

      setShowNameInputError(
        true
      )

    }
    if (!messageInputRef.current.value) {

      setShowMessageInputError(
        true
      )

    }
    if (!nameInputRef.current.value || !messageInputRef.current.value) return

    setShowNameInputError(
      false
    )
    setShowMessageInputError(
      false
    )

    window.location.href = `mailto:${email}?subject=${nameInputRef.current.value}&body=${messageInputRef.current.value}`

  }

  return (
    <div
      className='contactForm'
    >
      <Title
        label={formTitle}
      />
      <Input
        ref={nameInputRef}
        label={nameInputLabel}
        type='text'
      />
      {showNameInputError ? (
        <Paragraph
          label={nameInputError}
        />
      ) : null}
      <Input
        ref={messageInputRef}
        label={messageInputLabel}
        type='multiline'
      />
      {showMessageInputError ? (
        <Paragraph
          label={messageInputError}
        />
      ) : null}
      <NormalButton
        label={sendButtonLabel}
        onClick={onSend}
      />
    </div>
  )

}

ContactForm.propTypes = {
  formTitle: propTypes.string.isRequired,
  nameInputLabel: propTypes.string.isRequired,
  nameInputError: propTypes.string.isRequired,
  messageInputLabel: propTypes.string.isRequired,
  messageInputError: propTypes.string.isRequired,
  sendButtonLabel: propTypes.string.isRequired,
  email: propTypes.string.isRequired,
}

export default ContactForm
