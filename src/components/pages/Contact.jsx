import React from 'react'
import propTypes from 'prop-types'
import TwoRows from '../layouts/TwoRows'
import ContactForm from '../organisms/ContactForm'
import ContactInfoCard from '../molecules/ContactInfoCard'

const Contact = (
  {
    formTitle,
    nameInputLabel,
    nameInputError,
    messageInputLabel,
    messageInputError,
    sendButtonLabel,
    phoneNumberTitle,
    firstPhoneNumber,
    secondPhoneNumber,
    addressTitle,
    address,
    socialNetworksTitle,
    socialNetworks,
    email,
  }
) => {

  return (
    <div
      className='contact'
      id='contact'
    >
      <TwoRows
        firstRowLeftContent={(
          <ContactForm
            formTitle={formTitle}
            nameInputLabel={nameInputLabel}
            nameInputError={nameInputError}
            messageInputLabel={messageInputLabel}
            messageInputError={messageInputError}
            sendButtonLabel={sendButtonLabel}
            email={email}
          />
        )}
        firstRowRightContent={(
          <>
            <ContactInfoCard
              title={phoneNumberTitle}
              texts={[ firstPhoneNumber, secondPhoneNumber ]}
            />
            <ContactInfoCard
              title={addressTitle}
              texts={[ address ]}
            />
            <ContactInfoCard
              title={socialNetworksTitle}
              icons={[ ...socialNetworks ]}
            />
          </>
        )}
        secondRowImage='secondRowBackground.svg'
        secondRowContent={(
          <div>
            <iframe
              title='map'
              src='https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3798.4247421463965!2d-63.2144949844178!3d-17.818713080732923!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x93f1c365e7e59f87%3A0xbd6811d5e907b18a!2sTEMPLAVID!5e0!3m2!1sen!2sbo!4v1672976340799!5m2!1sen!2sbo'
              className='map'
              allowFullScreen=''
              loading='lazy'
              referrerpolicy='no-referrer-when-downgrade'
            />
          </div>
        )}
      />
    </div>
  )

}

Contact.propTypes = {
  formTitle: propTypes.string.isRequired,
  nameInputLabel: propTypes.string.isRequired,
  nameInputError: propTypes.string.isRequired,
  messageInputLabel: propTypes.string.isRequired,
  messageInputError: propTypes.string.isRequired,
  sendButtonLabel: propTypes.string.isRequired,
  phoneNumberTitle: propTypes.string.isRequired,
  firstPhoneNumber: propTypes.string.isRequired,
  secondPhoneNumber: propTypes.string.isRequired,
  addressTitle: propTypes.string.isRequired,
  address: propTypes.string.isRequired,
  socialNetworksTitle: propTypes.string.isRequired,
  socialNetworks: propTypes.arrayOf(
    propTypes.shape(
      {
        icon: propTypes.string.isRequired,
        url: propTypes.string.isRequired,
        label: propTypes.string.isRequired,
      }
    ).isRequired
  ).isRequired,
  email: propTypes.string.isRequired,
}

export default Contact
