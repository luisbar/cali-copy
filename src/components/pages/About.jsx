import React from 'react'
import PropTypes from 'prop-types'
import SuperTitle from '../atoms/SuperTitle'
import Paragraph from '../atoms/Paragraph'
import TwoRows from '../layouts/TwoRows'
import AboutItem from '../molecules/AboutItem'
import UnderlinedTitle from '../atoms/UnderlinedTitle'
import CardMembers from '../organisms/CardMembers'

const About = (
  {
    yearsOfExperience1,
    yearsOfExperience2,
    missionTitle,
    missionDescription,
    visionTitle,
    visionDescription,
    valuesTitle,
    valuesDescription,
    ourTeam,
    titleFirstTeamMember,
    nameFirstTeamMember,
    titleSecondTeamMember,
    nameSecondTeamMember,
  }
) => {

  return (
    <div
      className='about'
      id='about'
    >
      <TwoRows
        secondRowImage='secondRowBackground.svg'
        firstRowLeftContent={(
          <>
            <SuperTitle
              label={yearsOfExperience1}
            />
            <Paragraph
              label={yearsOfExperience2}
            />
          </>
        )}
        firstRowRightContent={(
          <>
            <AboutItem
              image='mission.svg'
              title={missionTitle}
              paragraph={missionDescription}
            />
            <AboutItem
              image='vision.svg'
              title={visionTitle}
              paragraph={visionDescription}
            />
            <AboutItem
              image='values.svg'
              title={valuesTitle}
              paragraph={valuesDescription}
            />
          </>
        )}
        secondRowContent={(
          <>
            <UnderlinedTitle
              label={ourTeam}
            />
            <CardMembers
              items={[
                {
                  image: 'teamMember.png',
                  title: titleFirstTeamMember,
                  name: nameFirstTeamMember,
                },
                {
                  image: 'teamMember.png',
                  title: titleSecondTeamMember,
                  name: nameSecondTeamMember,
                },
              ]}
            />
          </>
        )}
      />
    </div>
  )

}

About.propTypes = {
  yearsOfExperience1: PropTypes.string.isRequired,
  yearsOfExperience2: PropTypes.string.isRequired,
  missionTitle: PropTypes.string.isRequired,
  missionDescription: PropTypes.string.isRequired,
  visionTitle: PropTypes.string.isRequired,
  visionDescription: PropTypes.string.isRequired,
  valuesTitle: PropTypes.string.isRequired,
  valuesDescription: PropTypes.string.isRequired,
  ourTeam: PropTypes.string.isRequired,
  titleFirstTeamMember: PropTypes.string.isRequired,
  nameFirstTeamMember: PropTypes.string.isRequired,
  titleSecondTeamMember: PropTypes.string.isRequired,
  nameSecondTeamMember: PropTypes.string.isRequired,
}

export default About
