import React, { useState } from 'react'
import PropTypes from 'prop-types'
import Masonry, { ResponsiveMasonry } from 'react-responsive-masonry'
import HeaderAndBody from '../layouts/HeaderAndBody'
import UnderlinedTitle from '../atoms/UnderlinedTitle'
import Image from '../atoms/Image'
import WorkModal from '../molecules/WorkModal'

const Works = (
  { worksFeatures, items }
) => {

  const [ showWorkModal, setShowWorkModal ] = useState(
    false
  )
  const [ workModalData, setWorkModalData ] = useState(
    null
  )

  const onShowModal = (
    data
  ) => {

    return () => {

      setWorkModalData(
        data
      )
      setShowWorkModal(
        true
      )

    }

  }

  const onCloseModal = () => {

    setShowWorkModal(
      false
    )

  }

  return (
    <div
      className='works'
      id='works'
    >
      <HeaderAndBody
        header={(
          <UnderlinedTitle
            label={worksFeatures}
          />
        )}
        body={(
          <>
            <ResponsiveMasonry
              className='responsiveMasonry'
              columnsCountBreakPoints={{
                350: 1,
                750: 2,
                900: 3,
              }}
            >
              <Masonry
                gutter='10px'
              >
                {items.map(
                  (
                    { mainImage, ...rest }
                  ) => {

                    return (
                      <div
                        key={mainImage}
                        role='button'
                        onKeyPress={() => {}}
                        tabIndex='0'
                        onClick={onShowModal(
                          rest
                        )}
                      >
                        <Image
                          imageName={mainImage}
                        />
                      </div>
                    )

                  }
                )}
              </Masonry>
            </ResponsiveMasonry>
            <WorkModal
              show={showWorkModal}
              onClose={onCloseModal}
              {...workModalData}
            />
          </>
        )}
        bodyBackground='worksBackground.svg'
      />
    </div>
  )

}

Works.propTypes = {
  worksFeatures: PropTypes.string.isRequired,
  items: PropTypes.arrayOf(
    PropTypes.shape(
      {
        mainImage: PropTypes.string.isRequired,
        images: PropTypes.arrayOf(
          PropTypes.string.isRequired
        ).isRequired,
        title: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
      }
    )
  ).isRequired,
}

export default Works
