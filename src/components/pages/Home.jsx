import React from 'react'
import propTypes from 'prop-types'
import Centered from '../layouts/Centered'
import SuperTitle from '../atoms/SuperTitle'
import Paragraph from '../atoms/Paragraph'
import NormalButton from '../atoms/NormalButton'
import FloatingSocialNetworks from '../molecules/FloatingSocialNetworks'
import MadeBy from '../molecules/MadeBy'

const Home = (
  {
    superTitle, subTitle, button, socialNetworks, madeByTitle,
  }
) => {

  const onClick = () => {

    window.location.href = 'contact'

  }

  return (
    <div
      className='home'
      id='home'
    >
      <Centered
        image='homeBackground.png'
        body={(
          <>
            <SuperTitle
              label={superTitle}
            />
            <Paragraph
              label={subTitle}
            />
            <NormalButton
              label={button}
              onClick={onClick}
            />
            <FloatingSocialNetworks
              items={socialNetworks}
            />
            <MadeBy
              madeByTitle={madeByTitle}
            />
          </>
        )}
      />
    </div>
  )

}

Home.propTypes = {
  superTitle: propTypes.string.isRequired,
  subTitle: propTypes.string.isRequired,
  button: propTypes.string.isRequired,
  socialNetworks: propTypes.arrayOf(
    propTypes.shape(
      {
        icon: propTypes.string.isRequired,
        url: propTypes.string.isRequired,
        label: propTypes.string.isRequired,
      }
    ).isRequired
  ).isRequired,
  madeByTitle: propTypes.string.isRequired,
}

export default Home
