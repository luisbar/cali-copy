import React, { forwardRef } from 'react'
import propTypes from 'prop-types'
import Paragraph from '../atoms/Paragraph'

const Input = forwardRef(
  (
    { label, type }, ref
  ) => {

    const renderInput = () => {

      if (type === 'multiline') {

        return (
          <textarea
            ref={ref}
            rows={5}
          />
        )

      }

      return (
        <input
          ref={ref}
          type={type}
        />
      )

    }

    return (
      <div
        className='input'
      >
        <Paragraph
          label={label}
        />
        {renderInput()}
      </div>
    )

  }
)

Input.propTypes = {
  label: propTypes.string.isRequired,
  type: propTypes.string.isRequired,
}

export default Input
