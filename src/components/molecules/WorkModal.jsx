import React from 'react'
import PropTypes from 'prop-types'
import Slider from 'react-slick'
import Icon from '../atoms/Icon'
import Image from '../atoms/Image'
import Title from '../atoms/Title'
import Paragraph from '../atoms/Paragraph'

const WorkModal = (
  {
    show, onClose, images, title, description,
  }
) => {

  return (
    <div
      className={show ? 'workModal visible' : 'workModal'}
    >
      <div>
        <Icon
          iconName='faWindowClose'
          onClick={onClose}
        />
        <Slider
          className='slider'
          dots={false}
          arrows
          autoplay={false}
          speed={500}
          infinite
          slidesToShow={1}
          slidesToScroll={1}
          prevArrow={(
            <Icon
              iconName='faArrowCircleLeft'
            />
          )}
          nextArrow={(
            <Icon
              iconName='faArrowCircleRight'
            />
          )}
        >
          {images.map(
            (
              image
            ) => {

              return (
                <Image
                  key={image}
                  imageName={image}
                />
              )

            }
          )}
        </Slider>
        <Title
          label={title}
        />
        <Paragraph
          label={description}
        />
      </div>
    </div>
  )

}

WorkModal.propTypes = {
  show: PropTypes.bool.isRequired,
  onClose: PropTypes.func,
  images: PropTypes.arrayOf(
    PropTypes.string
  ),
  title: PropTypes.string,
  description: PropTypes.string,
}

WorkModal.defaultProps = {
  onClose: () => {},
  images: [],
  title: '',
  description: '',
}

export default WorkModal
