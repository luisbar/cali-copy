import React from 'react'
import propTypes from 'prop-types'
import Icon from '../atoms/Icon'

const FloatingSocialNetworks = (
  { items }
) => {

  const onClick = (
    { url }
  ) => {

    return () => {

      window.open(
        url, '_blank'
      )

    }

  }

  return (
    <div
      className='floatingSocialNetworks'
    >
      {items.map(
        (
          { icon, url, label }
        ) => {

          return (
            <div
              key={label}
              title={label}
              onClick={onClick(
                { url }
              )}
              role='button'
              onKeyPress={() => {}}
              tabIndex='0'
            >
              <Icon
                iconName={icon}
              />
            </div>
          )

        }
      )}
    </div>
  )

}

FloatingSocialNetworks.propTypes = {
  items: propTypes.arrayOf(
    propTypes.shape(
      {
        icon: propTypes.string.isRequired,
        url: propTypes.string.isRequired,
        label: propTypes.string.isRequired,
      }
    )
  ),
}

FloatingSocialNetworks.defaultProps = {
  items: [],
}

export default FloatingSocialNetworks
