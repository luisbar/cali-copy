import React from 'react'
import propTypes from 'prop-types'
import Paragraph from '../atoms/Paragraph'

const MadeBy = (
  { madeByTitle }
) => {

  const onClick = () => {

    window.open(
      'https://disruptivo.io', '_blank'
    )

  }

  return (
    <div
      className='madeBy'
    >
      <div
        className='top'
        onClick={onClick}
        role='button'
        onKeyPress={() => {}}
        tabIndex='0'
      >
        <Paragraph
          label={madeByTitle}
        />
      </div>
      <div
        className='bottom'
        onClick={onClick}
        role='button'
        onKeyPress={() => {}}
        tabIndex='0'
      >
        <Paragraph
          label='Disruptivo'
        />
      </div>
    </div>
  )

}

MadeBy.propTypes = {
  madeByTitle: propTypes.string.isRequired,
}

export default MadeBy
