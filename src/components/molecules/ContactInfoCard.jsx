import React from 'react'
import propTypes from 'prop-types'
import Title from '../atoms/Title'
import Paragraph from '../atoms/Paragraph'
import Icon from '../atoms/Icon'

const ContactInfoCard = (
  { title, texts, icons }
) => {

  const onClick = (
    { url }
  ) => {

    return () => {

      window.open(
        url, '_blank'
      )

    }

  }

  return (
    <div
      className='contactInfoCard'
    >
      <Title
        label={title}
      />
      {texts.map(
        (
          text
        ) => {

          return (
            <Paragraph
              key={text}
              label={text}
            />
          )

        }
      )}
      <div
        className='icons'
      >
        {icons.map(
          (
            { icon, url, label }
          ) => {

            return (
              <div
                key={label}
                title={label}
              >
                <Icon
                  iconName={icon}
                  onClick={onClick(
                    {
                      url,
                    }
                  )}
                />
              </div>
            )

          }
        )}
      </div>
    </div>
  )

}

ContactInfoCard.propTypes = {
  title: propTypes.string.isRequired,
  texts: propTypes.arrayOf(
    propTypes.string
  ),
  icons: propTypes.arrayOf(
    propTypes.shape(
      {
        icon: propTypes.string.isRequired,
        url: propTypes.string.isRequired,
        label: propTypes.string.isRequired,
      }
    )
  ),
}

ContactInfoCard.defaultProps = {
  texts: [],
  icons: [],
}

export default ContactInfoCard
