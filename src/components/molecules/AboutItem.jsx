import React from 'react'
import PropTypes from 'prop-types'
import Image from '../atoms/Image'
import Title from '../atoms/Title'
import Paragraph from '../atoms/Paragraph'

const AboutItem = (
  { image, title, paragraph }
) => {

  return (
    <div
      className='aboutItem'
    >
      <Image
        imageName={image}
      />
      <div>
        <Title
          label={title}
        />
        <Paragraph
          label={paragraph}
        />
      </div>
    </div>
  )

}

AboutItem.propTypes = {
  image: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  paragraph: PropTypes.string.isRequired,
}

export default AboutItem
