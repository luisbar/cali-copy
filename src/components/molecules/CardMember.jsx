import React from 'react'
import PropTypes from 'prop-types'
import Image from '../atoms/Image'
import Title from '../atoms/Title'
import Paragraph from '../atoms/Paragraph'

const CardMember = (
  { image, name, title }
) => {

  return (
    <div
      className='cardMember'
    >
      <Image
        imageName={image}
      />
      <Title
        label={name}
      />
      <Paragraph
        label={title}
      />
    </div>
  )

}

CardMember.propTypes = {
  image: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
}

export default CardMember
