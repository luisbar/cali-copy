---
about: {
  title: "Nosotros",
  yearsOfExperience1: "28 años",
  yearsOfExperience2: "de trayectoria",
  missionTitle: "MISIÓN",
  missionDescription: "Ofrecer un producto de calidad y un servicio de excelencia, con precios asequibles.",
  visionTitle: "VISIÓN",
  visionDescription: "Ser una empresa reconocida en el mercado local y nacional, en la comercialización e instalación de aluminio y vidrio.",
  valuesTitle: "VALORES",
  valuesDescription: "Puntualidad, seriedad, honestidad y responsabilidad",
  ourTeam: "Nuestro Equipo",
  titleFirstTeamMember: "Gerente General",
  nameFirstTeamMember: "Marcelo J. Poma Alcalá",
  titleSecondTeamMember: "Gerente de Finanzas",
  nameSecondTeamMember: "Pamela J. Poma Rua",
}
---