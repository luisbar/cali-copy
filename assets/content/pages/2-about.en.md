---
about: {
  title: "About",
  yearsOfExperience1: "28 years",
  yearsOfExperience2: "de trajectory",
  missionTitle: "MISSION",
  missionDescription: "To offer a product of highest quality and an excellent service, with affordable prices.",
  visionTitle: "VISION",
  visionDescription: "To be a recognized enterprise in the local an national market, in trading and installation of aluminium and glass.",
  valuesTitle: "VALUES",
  valuesDescription: "Punctuality, seriousness, honesty and accountable",
  ourTeam: "Our Team",
  titleFirstTeamMember: "CEO",
  nameFirstTeamMember: "Marcelo J. Poma Alcalá",
  titleSecondTeamMember: "CFO",
  nameSecondTeamMember: "Pamela J. Poma Rua",
}
---