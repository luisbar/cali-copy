---
works: {
  title: "Works",
  worksFeatures: "The most modern works cozy and safe",
items: [
    {
      mainImage: "building1.jpg",
      images: ["building1.jpg", "building2.jpg"],
      title: "Building 1",
      description: "I'm baby jianbing ennui vexillologist iPhone hoodie occupy. Keytar kitsch truffaut cornhole live-edge drinking vinegar mumblecore single-origin coffee waistcoat. Synth raw denim poke praxis ugh ennui echo park. Master cleanse chambray disrupt tumeric kinfolk umami pinterest. La croix before they sold out lo-fi Brooklyn adaptogen banjo vice squid fashion axe kickstarter blog truffaut DIY.",
    },
    {
      mainImage: "building2.jpg",
      images: ["building1.jpg", "building2.jpg"],
      title: "Building 2",
      description: "I'm baby jianbing ennui vexillologist iPhone hoodie occupy. Keytar kitsch truffaut cornhole live-edge drinking vinegar mumblecore single-origin coffee waistcoat. Synth raw denim poke praxis ugh ennui echo park. Master cleanse chambray disrupt tumeric kinfolk umami pinterest. La croix before they sold out lo-fi Brooklyn adaptogen banjo vice squid fashion axe kickstarter blog truffaut DIY.",
    },
    {
      mainImage: "building3.jpg",
      images: ["building1.jpg", "building2.jpg"],
      title: "Building 3",
      description: "I'm baby jianbing ennui vexillologist iPhone hoodie occupy. Keytar kitsch truffaut cornhole live-edge drinking vinegar mumblecore single-origin coffee waistcoat. Synth raw denim poke praxis ugh ennui echo park. Master cleanse chambray disrupt tumeric kinfolk umami pinterest. La croix before they sold out lo-fi Brooklyn adaptogen banjo vice squid fashion axe kickstarter blog truffaut DIY.",
    },
    {
      mainImage: "building4.jpg.webp",
      images: ["building1.jpg", "building2.jpg"],
      title: "Building 4",
      description: "I'm baby jianbing ennui vexillologist iPhone hoodie occupy. Keytar kitsch truffaut cornhole live-edge drinking vinegar mumblecore single-origin coffee waistcoat. Synth raw denim poke praxis ugh ennui echo park. Master cleanse chambray disrupt tumeric kinfolk umami pinterest. La croix before they sold out lo-fi Brooklyn adaptogen banjo vice squid fashion axe kickstarter blog truffaut DIY.",
    },
    {
      mainImage: "building5.jpg",
      images: ["building1.jpg", "building2.jpg"],
      title: "Building 5",
      description: "I'm baby jianbing ennui vexillologist iPhone hoodie occupy. Keytar kitsch truffaut cornhole live-edge drinking vinegar mumblecore single-origin coffee waistcoat. Synth raw denim poke praxis ugh ennui echo park. Master cleanse chambray disrupt tumeric kinfolk umami pinterest. La croix before they sold out lo-fi Brooklyn adaptogen banjo vice squid fashion axe kickstarter blog truffaut DIY.",
    },
    {
      mainImage: "building6.jpg",
      images: ["building1.jpg", "building2.jpg"],
      title: "Building 6",
      description: "I'm baby jianbing ennui vexillologist iPhone hoodie occupy. Keytar kitsch truffaut cornhole live-edge drinking vinegar mumblecore single-origin coffee waistcoat. Synth raw denim poke praxis ugh ennui echo park. Master cleanse chambray disrupt tumeric kinfolk umami pinterest. La croix before they sold out lo-fi Brooklyn adaptogen banjo vice squid fashion axe kickstarter blog truffaut DIY.",
    },
    {
      mainImage: "building7.jpg",
      images: ["building1.jpg", "building2.jpg"],
      title: "Building 7",
      description: "I'm baby jianbing ennui vexillologist iPhone hoodie occupy. Keytar kitsch truffaut cornhole live-edge drinking vinegar mumblecore single-origin coffee waistcoat. Synth raw denim poke praxis ugh ennui echo park. Master cleanse chambray disrupt tumeric kinfolk umami pinterest. La croix before they sold out lo-fi Brooklyn adaptogen banjo vice squid fashion axe kickstarter blog truffaut DIY.",
    },
    {
      mainImage: "building8.jpeg",
      images: ["building1.jpg", "building2.jpg"],
      title: "Building 8",
      description: "I'm baby jianbing ennui vexillologist iPhone hoodie occupy. Keytar kitsch truffaut cornhole live-edge drinking vinegar mumblecore single-origin coffee waistcoat. Synth raw denim poke praxis ugh ennui echo park. Master cleanse chambray disrupt tumeric kinfolk umami pinterest. La croix before they sold out lo-fi Brooklyn adaptogen banjo vice squid fashion axe kickstarter blog truffaut DIY.",
    },
  ],
}
---