---
contact: {
  title: "Contacto",
  formTitle: "Contactanos",
  nameInputLabel: "Nombre",
  nameInputError: "Nombre es obligatorio",
  messageInputLabel: "Mensaje",
  messageInputError: "Mensaje es obligatorio",
  sendButtonLabel: "Enviar",
  phoneNumberTitle: "Llamanos",
  firstPhoneNumber: "(+591) 65865221",
  secondPhoneNumber: "33537438",
  addressTitle: "Ubicación",
  address: "Avenida Doble vía 5to anillo, Barrio Flamingo, calle C-1 norte De Lunes a Viernes de 08:00 a 18:00",
  socialNetworksTitle: "Redes Sociales",
  socialNetworks: [
    {
      icon: "faWhatsapp",
      url: "https://wa.me/59165865221",
      label: "Whatsapp",
    },
    {
      icon: "faFacebookF",
      url: "https://www.facebook.com/TemplavidBo",
      label: "Facebook",
    },
    {
      icon: "faInstagram",
      url: "https://www.instagram.com/templavidbo/",
      label: "Instagram",
    },
  ],
  location: "-17.8187131,-63.214495",
  email: "templavid@gmail.com",
}
---