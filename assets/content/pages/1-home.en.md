---
home: {
  title: "Home",
  superTitle: "SPECIALIST",
  subTitle: "in provisions and installations of glass and aluminum",
  button: "Contact us",
  socialNetworks: [
    {
      icon: "faWhatsapp",
      url: "https://wa.me/59165865221",
      label: "Whatsapp",
    },
    {
      icon: "faFacebookF",
      url: "https://www.facebook.com/TemplavidBo",
      label: "Facebook",
    },
    {
      icon: "faInstagram",
      url: "https://www.instagram.com/templavidbo/",
      label: "Instagram",
    },
  ],
  madeByTitle: "Powered by"
}
---