---
home: {
  title: "Inicio",
  superTitle: "ESPECIALISTAS",
  subTitle: "en proviciones e instalaciones de vidrio y aluminio",
  button: "Contactanos",
  socialNetworks: [
    {
      icon: "faWhatsapp",
      url: "https://wa.me/59165865221",
      label: "Whatsapp",
    },
    {
      icon: "faFacebookF",
      url: "https://www.facebook.com/TemplavidBo",
      label: "Facebook",
    },
    {
      icon: "faInstagram",
      url: "https://www.instagram.com/templavidbo/",
      label: "Instagram",
    },
  ],
  madeByTitle: "Creado por",
}
---