module.exports = {
  title: 'Templavid | Carpintería de aluminio y vidrio',
  description: 'Templavid: Carpintería de aluminio y vidrio',
  author: 'Disruptivo',
  defaultLang: 'es',
  langTextMap: {
    es: 'Español',
    en: 'English',
  },
  keywords:[
    'templavid',
    'vidrio',
    'vidrios',
    'aluminio',
    'aluminios',
    'perfiles de aluminio',
    'carpintería de aluminio y vidrio',
    'carpintería',
  ],
  multiplePages: true,
  footerOrAside: true,
}
