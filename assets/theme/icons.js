import {
  faShoppingCart,
  faLaptop,
  faLock,
  faWindowClose,
  faArrowCircleLeft,
  faArrowCircleRight,
} from '@fortawesome/free-solid-svg-icons'

import {
  faTwitter,
  faFacebookF,
  faLinkedinIn,
  faGithubAlt,
  faMediumM,
  faWhatsapp,
  faInstagram,
} from '@fortawesome/free-brands-svg-icons'

import {
  faSurprise
} from '@fortawesome/free-regular-svg-icons'

export {
  faShoppingCart,
  faLaptop,
  faLock,
  faTwitter,
  faFacebookF,
  faLinkedinIn,
  faGithubAlt,
  faMediumM,
  faSurprise,
  faWindowClose,
  faArrowCircleLeft,
  faArrowCircleRight,
  faWhatsapp,
  faInstagram,
}